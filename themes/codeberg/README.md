# codeberg

A custom [pelican][pl] theme for the Codeberg blog. It's based on the
[aboutwilson][aw] theme with some minor tweaks, which are listed below.

* Updated to FontAwesome 5
* Reduced number of icons
* Different font family for body and headings
* Changed colour theme to blue
* "Tags: " text is only shown if the post has tags
* Site title is aligned with the post titles
* New `STARTYEAR` pelican config option

[pl]: https://getpelican.com
[aw]: https://github.com/getpelican/pelican-themes/tree/master/aboutwilson

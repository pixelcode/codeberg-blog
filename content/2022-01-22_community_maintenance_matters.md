Title: Community Maintenance Matters
Date: 2022-01-22
Category: Contrib
Authors: Otto Richter (Presidium member of Codeberg e. V.)


When we at Codeberg are asked to compare ourselves to other platforms, this often includes a lot of technical blah-blah about enabled features, and we have to be honest here:
We don't yet always meet expectations by people moving from proprietary platforms, which are ahead in features.
Although Gitea is a very nice tool and making rapid progress day to day, there are thousands of open issues and feature requests, bug reports and much more to solve.

Still, more and more people chose Codeberg as their home for Free and Libre Open Source Software.
Some, because they like the less-cluttered interface. Others because they want privacy or live in embargoed countries.
But many are starting to care about *who* runs their system on the web, which is a question becoming more and more important in times when the growth of the internet and platforms therein is moving at rapid pace.

In this blog post, I want to outline my very personal motivation for Codeberg.org, and explain why I consider our model as key for sustainable platforms in the 21st century.


## Self-Host all the things!?

If you use cloud software, you always need to keep in mind this is just some tool that runs on someone else's computer.
They physically own it, they can control it and the software it runs, the data it stores and the communication it does.
So why rely on others to run the service for you, if you can also host it yourself?

I completely get that motivation, and I share it to some extent.
When I was 15, I really wanted to have my very own Etherpad server, I considered this software awesome back then (and still use it at times).
Still, I soon had to learn this was harder than I expected.
Only years later I was able to finally get the server up and running, damn, and since then I hate the Node JS ecosystem for being so fragile IMO.
That aside, there are many many people out there who can't run their own servers, whether they lack the skills, the time, or the money.
There will always be a group of people who have to rely on other's service, and for them, we need to make a good offer.

I'm proud of hosting stuff myself. And I'm proud I see others using it, often people I don't even know use my servers and do (probably) cool stuff on it.
But this does put some stress on me.
Back when I set them up, I had some spare time maintaining my systems, and it would not matter if they break a day, or two, or even a week.
Now, I feel responsible for keeping them up, but my time didn't grow accordingly.
I really feel guilty of not updating some software components in ages, because an upgrade broke and I only rolled back without the time to properly review.
What if there was a security issue? What, if user data had been lost?
I do a lot better now, but the bad taste still stays in my mouth.
How can I, as a single person, responsibly host something for the world?
How can others, trust me as a single person, to keep the system up and running?

A good example for self-hosting is probably the Fediverse, especially the many many Mastodon instances out there.
But when choosing one of them, you need to trust the owner to keep it up for you.
Many of them don't even carry an impress, don't provide a way to contact anyone, and don't make any guarantees.
Fine for me - sometimes. Sometimes it's not, and I need a tool that just works when I need it. Really.


## Why don't you just pay for it?

A popular opinion I see here and there is to just pay people for a service, often in combination with developing software in common.
In theory, this sounds very good: For example, I like how many companies started offering BigBlueButton hosting in the Coronavirus pandemic.
I really prefer this over paying for companies that develop or resale closed source software components, and this practice is still way too common nowadays.
While this might work in most cases, especially for co-operative societies and small start-ups, sometimes I feel like I want even more control over my data and demand transparency about what happens to my money.

We are quite happy with our decision to choose Gitea over GitLabCE.
It is a lot of fun to communicate with the maintainer team, and we know it is a truly community-maintained effort to maximize the value for the users, not for profit.
Decisions are not influenced by big companies, and the developers are free to evaluate what is really the best direction for the project.
The Same would apply for a company hosting a website. They still need to make decisions about settings and stuff.
Listening to users as a company is one thing, allowing your users to be part of the decision-making process is the next level.


## Democracy for the win

What makes Codeberg kind-of unique among large platforms is the community maintenance model.
Codeberg.org is backed by the non-profit Codeberg e.V., which is legally bound to its goal to support the development of Free and Open Source Software.
By joining the association, you gain voting rights and elect the presidium and indirectly the board, which acts in the interest of the members.
People are allowed to vote on certain matters, especially during the virtual member assemblies.

Since they can take part and influence the direction of the platform, I'd argue they even "own" it.
Who, if not them, as Codeberg is not affiliated with a company and no private person can arbitrarily take control over it.
Codeberg always acts in the interest of its users, and in case of disagreement, democratic processes will lead to a decision.


## Efficiency

Lastly, I want to share some thoughts on efficiency.
Although the Internet gives the impression that we have overcome physical limits, being able to easily copy and distribute data to many people with close to no cost, this is not ultimately true.
We need to buy or rent hardware, pay for energy and internet access.
We should consider how to keep the impact on the environment as minimal as possible, if we want to continue living on this planet for more than 20 more years.
And we have to keep an eye on our time, as administrating systems, caring for users, fighting spam and developing the software all takes time.

When I'm talking about efficient computer systems, I often have our CPU usage graph of Codeberg in mind.
A year ago, we would see a low baseline with occasional spikes.
When running more small servers, you need to make sure to have plenty of headroom for large push/pull actions, while the server would otherwise idle.
Now, with Codeberg growing, we are getting more and more balanced over time.
The baseline is higher, spikes are less noticeable, and we are using our computing resources more efficiently.

In 2021, we were able to finally buy own hardware, which also allowed us to reduce the cost per project / user on Codeberg.
It's also nice to know that your donations unfold maximum impact this way.

As I mentioned above in the long part about self-hosting, running a software can be very time-consuming, especially if you take care of a multitude of services.
The showcase example "Mastodon" outlines a few issues of this.
Most instance operators, who are offering public access and granting some insights, explain that most of their time is spent on that single service.
They are often a single person in charge of the instance or multiple services.

While you can of course dedicate your life into hosting one service for the world, you'll certainly not find the time to do anything to improve the software itself.
As someone who is mainly caring for the day-to-day operation of Codeberg, I find not close as much time as I want to get my hands on and type in some code for Gitea or other parts of Codeberg.
But happily, I'm not alone in the Codeberg team, and while I and others keep the systems up and running, the rest of the team can work out new features, fix bugs, or write a new Codeberg Pages Server for everyone to use.
Imagine each of us would take care of only one Gitea instance, all of them would be far from providing the same level of user experience as Codeberg does.


## A mission for the world

Recently, someone on Mastodon said the world needs many Codebergs, and I can fully second that mission.
Our primary goal is and will always be fighting monopoly, and breaking the idea that software development equals Git equals Microsoft's proprietary platform.
And although Codeberg is far more libre, we are also not for everyone.

To reach an ultimate level of efficiency for Codeberg, I expect we require times 5 to 50 our current user count, that is 100k to 1 million.
And if you fear the monopoly: this still leaves room for 73 to 730 instances if we want to replace Microsoft GitHub alone.

So I really hope we can get there, with a federated Gitea hopefully coming soon.
With proud self-hosters forming associations and non-profits, to combine the forces and provide better service.
Platforms that are owned by users, doing the best for them, and not for investors.
Our funds going into improving this ecosystem, rather than sustaining big business around the world.
And everyone keeping the freedom to still self-host if they really need or want this, for their family and friends, their company and high school.


## From utopy to practice

Lastly, I want to say a big thank you to everyone who is already making this possible.
To the team behind Gitea who is making the hosting of the software so seamless that saves us and others time and headache.
To all the people behind Codeberg who are in this mission together, whether in day-to-day operation, by backing the association as registered members, or contributing to the various projects we are running.
And of course to all the other associations, small and large, who are already living for a similar mission, among others:

- [Anoxinon on Codeberg](https://codeberg.org/Anoxinon_e.V./), offering several services like Mastodon, XMPP, Cloud
- [Softwerke Magdeburg on Codeberg](https://codeberg.org/softwerke-magdeburg/), a local initiative in Magdeburg, Germany with similar efforts
- [Dis`root´](https://disroot.org/), providing many free services, including maximum privacy (and a Gitea instance, too)
- [the Wikimedia Foundation](https://wikimediafoundation.org/), inspiring us that impact in the world is possible and not limited to the largest companies

Title: Community Spotlight: MoeNavigator
Date: 2021-01-11
Category: Community Spotlight
Authors: fnetX


Hi there, welcome to the Codeberg Community Spotlight. This is the first blog post in the upcoming series titled "Codeberg Community Spotlight". Are you as curious as we are, looking frequently at those active projects in the "[Explore](https://codeberg.org/explore/repos)" tab? I do, and it's a lot of fun to check out other's work, try it, give feedback and spread motivation ... that's the Community part of a development Community, isn't it?
The Community Spotlight extends on this and dives into some projects from time to time. Have fun reading!

In case you have some feedback for us as well, feel free to join the Discussion at [Codeberg/Community#200](https://codeberg.org/Codeberg/Community/issues/200). The format is not fixed at all, and at least for me it's just an expression of my deep interest in others work. If your fingers itch to present your own discovery of a Codeberg project, feel free to create an MR to the blog.

For this very first post, I randomly picked a project from the Explore timeline at January 09 that sounded interesting. Since I took part in many discussions about alternative web browsers recently, I picked this one: MoeNavigator, a web browser engine from scratch.


## MoeNavigator
The oligopoly of the few available web browsers is a tiresome topic. Many discussions have arisen whether the "free" Firefox and Chromium browsers are really free, not to mention the proprietary ones.

This is where the MoeNavigator steps in, a new web browser including its own engine, completely written from scratch. Of course, creating a working web renderer is a very complex thing, that's why maintainer started working on this project more than eight years ago. Don't expect too much.

Checking out the project itself was very straightforward. The README is rather implicit than full of explanations. I missed some notes on the dependencies and how to compile exactly, but both were added just a few hours later, so I'm glad I didn't bother filing an issue. There are wiki repositories for both the Engine and the Navigator which randomly answer some questions and give an overview about the architecture and what's planned. I really like including the wikis as a git submodule, I'll definitely consider doing so for my own repositories, too.


### Now get to the software!
The project is separated into two components: the MoeNavigatorEngine (MNE) and the MoeNavigator itself. There is a third repository with some tools I won't describe any further. 

The software itself is implemented in C++ with a custom rendering implementation. Compared to mature browsers (which surely consumed uncountable working hours by thousands of contributors), this project is rather tiny with about 12k source lines of code. The user interface is implemented in Qt. Maintainer Moritz Strohm to the decision of the Qt company to close down Qt5.15 LTS source code for commercial use:
> I think in the long term, the Qt Company is going the path of Oracle with
> decisions like this. I see this case as one of the attempts to prevent the
> free software community from actually using free software. Or in other words:
> This is one attempt of the ruling class to take away a mean of production (Qt)
> from the working class. That attempt has to be rejected.
> 
> While the Qt framework may remain free as in free speech, it will probably
> become harder to exercise that freedom. Unstable library APIs will make it
> harder for developers that write software in their free time to keep it updated
> since every new API version can break things. In the long-term it means that
> if such attempts like the one of the Qt Company aren't rejected and reversed,
> only companies with enough money or manpower will be able to use the freedoms
> of the software. This is why a fork or replacement of Qt is very important
> at the moment. It would be a strong message to companies such as the Qt Company
> to tell them that they can't just take the benefits from free software without
> having to give back to the community.

Back to MoeNavigator. Looking at the disputed UI framework in action, you'll experience MoeNavigator like this:

<div class="row">
	<img class="col-lg-6"
		src="/images/community_spotlight_2021-01_moenavigator.png"
		alt='Screenshot of the MoeNavigator Interface:
		Small window with toolbar: the first menu entry "Navigate" is usable,
		the others like Edit, View, Page, Options are still grey.
		Below are some buttons to go forward, backward, create a new tab and enter
		the URL. 
		The current URL is https://codeberg.org/robots.txt
		The selected tab is called "empty". It shows the robots dot t x t file of 
		Codeberg dot org in a single line.
		At the bottom of the window are some still unusable buttons and options, 
		among them a selector between "session" and "domain", and switches to toggle 
		Tor, Images, JavaScript, Audio, Video etc.'>
	<div class="col-lg-6">
		Well, you can see that the rendering is far from perfect. Until some fixes to the CSS parser, only websites with a single text line can be rendered. That said, MoeNavigator is unlikely to replace your current browser yet. But, heck, let's see what this project evolves to. More interesting than the rendering features is the network stack of the MNE. It was rewritten last year to have flexible support for multiple protocols, in addition to HTTP/s, gopher and qotd are supported currently with gemini protocol support to come next.
	</div>
</div>

<br/>

### What is to come?
The very first real release with the motto "[Uyghur lives matter!](https://codeberg.org/moenavigator/moenavigatorengine/milestone/41)" will bring the desired fixes to the CSS engine and some additional improvements. After that, Gemini support is expected, which requires "some additional work on the internals". Whether to stick to Qt or transition to another toolkit is also an upcoming decision.

> Helping hands are welcome, whether it is writing bug reports, feature requests,
> creating merge requests, writing documentation or writing tests.

So take that invitation serious and check it out. There are some memory leaks in the engine currently that need fixing, and oh yes: the CSS parser that already mentioned. Of course, it is a long journey until the webbrowser is usable, but I do not doubt that the experiences are worth it. The project is surely interesting and landed on my watchlist, although my experiences in C++ don't seem to qualify for a code contribution yet. I wish the project all the best!

### About the maintainer: Moritz Strohm
The maintainer of this project, [@ncc1988](https://codeberg.org/ncc1988), also known as Moritz Strohm, started coding out of the curiosity to find out how things work in a computer. Besides his job in a small German software company, where he is dedicated to create a free and libre open source campus and learning management system, he spends a lot of time with this and other personal projects that don't just include coding but also [electronics](https://codeberg.org/ncc1988/open-system-compact-cassette). The balance comes through cycling, hiking and reading.

**If you were to start from scratch today, what would you do differently?**
> I would first think of the basic project architecture and then start programming
> so that I don't have to refactorise the whole foundation to make the code
> extendable.

The work on this project was also part of his bachelor thesis and gave him a lot of experience over the past years. To answer the question of how difficult it would be to write a webbrowser from scratch might somehow have turned into a life task to the more than 30 year old developer.

> In general, I gained a lot of experience by writing this project [...] But there is
> still a lot to learn.


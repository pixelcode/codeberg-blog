Title: Monthly Report November 2020
Date: 2020-12-09
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

We are hosting 7618 repositories, created and maintained by 6156 users. Compared to one month ago, this is an organic growth rate of +1031 repositories (+15.7% month-over-month) and +763 users (+14.1%).

A week ago we also moved codeberg.org to a new server. We tried to minimize the downtime and make the transition as smooth as possible. This also involved a tunnel on the old server for those who still got old DNS responses, which we kept open until today. If have problems accessing codeberg.org, please contact us.

The machines have been upgraded and are runnnig at about 21% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 91 members in total, these are 70 members with active voting rights and 21 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 


Title: Monthly Report October 2021
Date: 2021-11-16
Category: Monthly Letter
Authors: Otto Richter (Codeberg e.V.)

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!


We are hosting 17900 repositories, created and maintained by 15572 users. Compared to one month ago, this is an organic growth rate of +1413 repositories (+8.6% month-over-month) and +1027 users (+7.1%).

**Feedback collection:** We are currently running two discussions between members of the Codeberg e.V. If you are a member, check the internal discussion tracker and raise your voice. If not, feel free to [join us](https://join.codeberg.org).

**CI status:** The closed alpha is going great so far. We have ~60 projects signed up, not all of them have configured access by now. We prepare to broaden access through ongoing development and beta launch. Please sign up to join the alpha/beta test program. In October 2021, there was a lot of progress including major updates to woodpecker UI. Performance tests on spinning hard drives instead of SSD showed very similar performance characteristics, enabling lower cost operation by reducing our dependence to costly SSDs prone to weardown.


The machines are running at about 48% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 148 members in total, these are 110 members with active voting rights and 37 supporting members, and one honorary member.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.
